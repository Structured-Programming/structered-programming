length = int(input("Hoe groot? "))

# Pyramide links naar rechts
for x in range(1, length + 1):
    print("*"*x)

for x in range(length - 1, -1, -1):
    print("*"*x)

# Pyramide links naar rechts
x = 0
while x < length:
    x += 1
    print("*"*x)

x = length
while 1 < x:
    x -= 1
    print("*"*x)


# Pyramide rechts naar links
for x in range(1, length + 1):
    for y in range(length - x):
        print(' ', end='')

    print("*"*x)

for x in range(length - 1, -1, -1):
    for y in range(length - x):
        print(' ', end='')

    print("*"*x)

# Pyramide rechts naar links
x = 1
while x <= length:
    y = length - x
    while y > 0:
        print(' ', end='')
        y -= 1

    print("*" * x)
    x += 1

x = length - 1
while 0 < x:
    y = length - x
    while y > 0:
        print(' ', end='')
        y -= 1

    print("*"*x)
    x -= 1



