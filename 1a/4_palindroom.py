words = ['racecar', 'legovogel', 'python', 'redder', 'palindroom']

def checkPalindroomWithLib(word):
    reversedWord = list(reversed(word))
    return word == "".join(reversedWord)


def checkPalindroom(word):
    return word == word[::-1]


for word in words:
    print(checkPalindroomWithLib(word))
