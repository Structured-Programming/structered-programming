lst = [4, 9, 16, 8, 14, 6, 19, 13, 2, 4, 11, 18, 12, 17, 10, 1, 4, 7, 20, 3, 5, 15, 3]
binary_lst = [0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1]

def count(num, lst):
    counter = 0
    for item in lst:
        if num == item:
            counter += 1

    return counter


def diff(lst):
    return max(lst) - min(lst)


def reviewList(lst):
    if count(1, binary_lst) < count(0, binary_lst):
        print('Er zijn minder enen dan nullen...')
        return False

    if count(0, binary_lst) > 12:
        print('Er zijn meer dan 12 nullen aanwezig...')
        return False

    print('De lijst voldoet aan alle eisen!')
    return True

reviewList(lst)
