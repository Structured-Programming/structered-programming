uncompressedFile = open('files/uncompressed.txt', 'r')
uncompressed = uncompressedFile.readlines()

compressedFile = open('files/compressed.txt', 'w')

for line in uncompressed:
    if not line.isspace():
        compressedFile.write(line.strip(' '))
