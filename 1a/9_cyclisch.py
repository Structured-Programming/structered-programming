def shift(ch, n):
    return ch[-n:] + ch[:-n]

print(shift('1011000', 3))
