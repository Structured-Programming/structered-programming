alphabets = 'abcdefghijklmnopqrstuvwxyz'
alphabets_upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

text = input('Geef een tekst:')
rotation = int(input('Geef een rotatie:'))
text_output = ''

for char in text:
    if char.islower():
        current_pos = alphabets.find(char)
    else:
        current_pos = alphabets_upper.find(char)

    if current_pos >= 0:
        new_pos = (current_pos + rotation) % 26

        if char.islower():
            text_output += alphabets[new_pos]
        else:
            text_output += alphabets_upper[new_pos]
    else:
        text_output += char

print(f'Input: {text}')
print(f'Rotation: {rotation}')
print(f'Output: {text_output}')
