fibonaci_lst = [0, 1]

def fibonaci(n):
    if len(fibonaci_lst) > n:
        return fibonaci_lst[-1]

    new_element = fibonaci_lst[-2] + fibonaci_lst[-1]
    fibonaci_lst.append(new_element)
    return fibonaci(n)

fibonaci(9)
