import random

randomNumber = random.randint(0, 10)

print('Kies een nummer tussen 0 en 10')

while True:
    guess = int(input('Maak je gok:'))
    if guess == randomNumber:
        print('Goed gegokt!')
        break
    else:
        print(f'{guess} is niet goed...')
